val tuPrologVersion: String by project

dependencies {
    api(project(":theory-geo"))
    api("it.unibo.tuprolog:solve-classic-jvm:$tuPrologVersion")
}
