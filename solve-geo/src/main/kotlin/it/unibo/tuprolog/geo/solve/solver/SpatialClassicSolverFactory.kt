package it.unibo.tuprolog.geo.solve.solver

import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.region.Region
import it.unibo.tuprolog.geo.theory.region.RegionFactory
import it.unibo.tuprolog.geo.theory.region.RegionFactoryParam
import it.unibo.tuprolog.geo.theory.theory.impl.Tile38MutableSpatialTheory
import it.unibo.tuprolog.geo.theory.theory.impl.Tile38SpatialTheory
import it.unibo.tuprolog.solve.MutableSolver
import it.unibo.tuprolog.solve.Solver
import it.unibo.tuprolog.solve.SolverFactory
import it.unibo.tuprolog.solve.channel.InputChannel
import it.unibo.tuprolog.solve.channel.OutputChannel
import it.unibo.tuprolog.solve.classic.stdlib.DefaultBuiltins
import it.unibo.tuprolog.solve.exception.PrologWarning
import it.unibo.tuprolog.solve.flags.FlagStore
import it.unibo.tuprolog.solve.library.AliasedLibrary
import it.unibo.tuprolog.solve.library.Libraries
import it.unibo.tuprolog.theory.Theory

object SpatialClassicSolverFactory : SolverFactory {

     val defaultRegion: Region = RegionFactory.getRegion(
        "default",
        RegionFactoryParam.BoundParam(
            GeoLocation(-110.0, 30.0),
            GeoLocation(-100.0, 40.0)
        ))

    override val defaultBuiltins: AliasedLibrary
        get() = DefaultBuiltins

    override val defaultStaticKb: Tile38SpatialTheory
        get() = Tile38SpatialTheory(defaultRegion, mapOf(), "redis://localhost:9851")

    override val defaultDynamicKb: Tile38MutableSpatialTheory
        get() = Tile38MutableSpatialTheory(defaultRegion, mapOf(), "redis://localhost:9851")

    override fun mutableSolverOf(
        libraries: Libraries,
        flags: FlagStore,
        staticKb: Theory,
        dynamicKb: Theory,
        stdIn: InputChannel<String>,
        stdOut: OutputChannel<String>,
        stdErr: OutputChannel<String>,
        warnings: OutputChannel<PrologWarning>
    ): MutableSolver {
        TODO("not implemented")
    }

    override fun solverOf(
        libraries: Libraries,
        flags: FlagStore,
        staticKb: Theory,
        dynamicKb: Theory,
        stdIn: InputChannel<String>,
        stdOut: OutputChannel<String>,
        stdErr: OutputChannel<String>,
        warnings: OutputChannel<PrologWarning>
    ): Solver = SpatialClassicSolver(libraries, flags, staticKb, dynamicKb, stdIn, stdOut, stdErr, warnings)

    fun spatialSolverWithDefaultBuiltins(
        otherLibraries: Libraries,
        flags: FlagStore,
        staticKb: Theory,
        dynamicKb: Theory,
        stdIn: InputChannel<String>,
        stdOut: OutputChannel<String>,
        stdErr: OutputChannel<String>,
        warnings: OutputChannel<PrologWarning>
    ): Solver = solverOf(
        otherLibraries + defaultBuiltins,
        flags,
        staticKb,
        dynamicKb,
        stdIn,
        stdOut,
        stdErr,
        warnings
    )
}

