package it.unibo.tuprolog.geo.solve.solver

import it.unibo.tuprolog.solve.Solver
import it.unibo.tuprolog.solve.channel.InputChannel
import it.unibo.tuprolog.solve.channel.OutputChannel
import it.unibo.tuprolog.solve.classic.ClassicSolverFactory
import it.unibo.tuprolog.solve.exception.PrologWarning
import it.unibo.tuprolog.solve.flags.FlagStore
import it.unibo.tuprolog.solve.library.Libraries
import it.unibo.tuprolog.theory.Theory

fun Solver.Companion.spatialSolverWithDefaultBuiltins(
    libraries: Libraries = ClassicSolverFactory.defaultLibraries,
    flags: FlagStore = ClassicSolverFactory.defaultFlags,
    staticKb: Theory = SpatialClassicSolverFactory.defaultStaticKb,
    dynamicKb: Theory = SpatialClassicSolverFactory.defaultDynamicKb,
    stdIn: InputChannel<String> = ClassicSolverFactory.defaultInputChannel,
    stdOut: OutputChannel<String> = ClassicSolverFactory.defaultOutputChannel,
    stdErr: OutputChannel<String> = ClassicSolverFactory.defaultErrorChannel,
    warnings: OutputChannel<PrologWarning> = ClassicSolverFactory.defaultWarningsChannel
): Solver =
    SpatialClassicSolverFactory.spatialSolverWithDefaultBuiltins(libraries, flags, staticKb, dynamicKb, stdIn, stdOut, stdErr, warnings)

fun Solver.Companion.spatialSolver(
    libraries: Libraries = ClassicSolverFactory.defaultLibraries,
    flags: FlagStore = ClassicSolverFactory.defaultFlags,
    staticKb: Theory = SpatialClassicSolverFactory.defaultStaticKb,
    dynamicKb: Theory = SpatialClassicSolverFactory.defaultDynamicKb,
    stdIn: InputChannel<String> = ClassicSolverFactory.defaultInputChannel,
    stdOut: OutputChannel<String> = ClassicSolverFactory.defaultOutputChannel,
    stdErr: OutputChannel<String> = ClassicSolverFactory.defaultErrorChannel,
    warnings: OutputChannel<PrologWarning> = ClassicSolverFactory.defaultWarningsChannel
): Solver =
    SpatialClassicSolverFactory.solverOf(libraries, flags, staticKb, dynamicKb, stdIn, stdOut, stdErr, warnings)
