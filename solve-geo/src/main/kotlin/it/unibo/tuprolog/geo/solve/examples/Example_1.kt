package it.unibo.tuprolog.geo.solve.examples

import it.unibo.tuprolog.core.Scope
import it.unibo.tuprolog.core.Substitution

/**
 *
 *
 * @author Lorenzo
 */
fun main() {
    Scope.empty {
        val term = structOf("father", varOf("X"), atomOf("isaac"))
        val substitution = Substitution.Companion.of(varOf("X") to atomOf("abraham"))

        println(term)
        println(substitution)
        val result = substitution.applyTo(term)

        println(result)
    }
    /*
    Scope.empty {
        val substitution = Substitution.of(
            varOf("X") to varOf("Y"),
            varOf("Y") to varOf("Z")
        )

        val originalZ = substitution.getOriginal(varOf("Z"))

        println(substitution)
        println(originalZ)
    }*/
    /*
    Scope.empty {
        val term = structOf("father", varOf("X"), varOf("Y"))

        val sub1 = Substitution.of(varOf("X"), atomOf("abraham"))
        val sub2 = Substitution.of(varOf("Y"), atomOf("isaac"))

        val substitution = sub1 + sub2

        val result = substitution.applyTo(term)
        println(result)
    }*/
    /*Scope.empty {
        val term = structOf("father", varOf("X"), atomOf("isaac"))

        val sub1 = Substitution.of(varOf("X"), atomOf("abraham"))
        val sub2 = Substitution.of(varOf("X"), atomOf("nahor"))

        val substitution = sub1 + sub2 // contradiction!
        val result = substitution.applyTo(term) // father(X_0, isaac) (substitution could not be performed)

        println(substitution)
        println(result)
    }*/

    Scope.empty {
        val substitution = Substitution.of(
            varOf("X") to atomOf("abraham"),
            varOf("Y") to atomOf("isaac")
        )

        println(substitution)
    }
}
