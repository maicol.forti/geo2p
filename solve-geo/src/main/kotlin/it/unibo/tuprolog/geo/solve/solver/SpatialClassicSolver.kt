package it.unibo.tuprolog.geo.solve.solver

import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.geo.theory.region.Region
import it.unibo.tuprolog.geo.theory.theory.impl.Tile38MutableSpatialTheory
import it.unibo.tuprolog.geo.theory.theory.impl.Tile38SpatialTheory
import it.unibo.tuprolog.solve.Solution
import it.unibo.tuprolog.solve.SolveOptions
import it.unibo.tuprolog.solve.channel.InputChannel
import it.unibo.tuprolog.solve.channel.InputStore
import it.unibo.tuprolog.solve.channel.OutputChannel
import it.unibo.tuprolog.solve.channel.OutputStore
import it.unibo.tuprolog.solve.classic.AbstractClassicSolver
import it.unibo.tuprolog.solve.classic.MutableSolutionIterator
import it.unibo.tuprolog.solve.classic.fsm.State
import it.unibo.tuprolog.solve.classic.fsm.StateRuleSelection
import it.unibo.tuprolog.solve.exception.PrologWarning
import it.unibo.tuprolog.solve.flags.FlagStore
import it.unibo.tuprolog.solve.library.Libraries
import it.unibo.tuprolog.theory.MutableTheory
import it.unibo.tuprolog.theory.Theory

internal open class SpatialClassicSolver : AbstractClassicSolver {

    private var region: Region = SpatialClassicSolverFactory.defaultRegion

    constructor(
        libraries: Libraries = Libraries.empty(),
        flags: FlagStore = FlagStore.empty(),
        initialStaticKb: Theory = SpatialClassicSolverFactory.defaultStaticKb,
        initialDynamicKb: Theory = SpatialClassicSolverFactory.defaultDynamicKb,
        inputChannels: InputStore = InputStore.default(),
        outputChannels: OutputStore = OutputStore.default(),
        trustKb: Boolean = true
    ) : super(libraries, flags, initialStaticKb, initialDynamicKb, inputChannels, outputChannels, trustKb)

    constructor(
        libraries: Libraries = Libraries.empty(),
        flags: FlagStore = FlagStore.empty(),
        staticKb: Theory = SpatialClassicSolverFactory.defaultStaticKb,
        dynamicKb: Theory = SpatialClassicSolverFactory.defaultDynamicKb,
        stdIn: InputChannel<String> = InputChannel.stdIn(),
        stdOut: OutputChannel<String> = OutputChannel.stdOut(),
        stdErr: OutputChannel<String> = OutputChannel.stdErr(),
        warnings: OutputChannel<PrologWarning> = OutputChannel.warn(),
        trustKb: Boolean = true
    ) : super(libraries, flags, staticKb, dynamicKb, stdIn, stdOut, stdErr, warnings, trustKb)

    override fun solutionIterator(
        initialState: State,
        onStateTransition: (State, State, Long) -> Unit
    ) = MutableSolutionIterator.of(initialState, this::hijackStateTransition, onStateTransition)

    final override fun solveOnce(goal: Struct, options: SolveOptions): Solution {
        if (options.customOptions.containsKey("Region")) {
            region = options.customOptions["Region"] as Region
        }
        return super.solveOnce(goal, options)
    }

    private fun hijackStateTransition(source: State, destination: State, index: Long): State {
        if (destination is StateRuleSelection) {
            return destination.copy(
                context = destination.context.copy(
                    staticKb = hijackStaticKb(destination.context.staticKb),
                    dynamicKb = hijackDynamicKb(destination.context.dynamicKb)
                )
            )
        }
        return destination
    }

    private fun hijackStaticKb(staticKb: Theory): Theory =
        (staticKb as Tile38SpatialTheory).setRegion(region)

    private fun hijackDynamicKb(dynamicKb: MutableTheory): MutableTheory =
        (dynamicKb as Tile38MutableSpatialTheory).setRegion(region)

    override fun copy(
        libraries: Libraries,
        flags: FlagStore,
        staticKb: Theory,
        dynamicKb: Theory,
        stdIn: InputChannel<String>,
        stdOut: OutputChannel<String>,
        stdErr: OutputChannel<String>,
        warnings: OutputChannel<PrologWarning>
    ) = SpatialClassicSolver(
        libraries,
        flags,
        staticKb,
        dynamicKb,
        stdIn,
        stdOut,
        stdErr,
        warnings
    )

    override fun clone(): SpatialClassicSolver = copy()
}
