package it.unibo.tuprolog.geo.solve.examples

import it.unibo.tuprolog.dsl.theory.prolog
import it.unibo.tuprolog.geo.solve.solver.spatialSolverWithDefaultBuiltins
import it.unibo.tuprolog.geo.theory.region.*
import it.unibo.tuprolog.geo.theory.region.impl.Bounds
import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.tile38.Tile38Connection
import it.unibo.tuprolog.solve.Solution
import it.unibo.tuprolog.solve.SolveOptions
import it.unibo.tuprolog.solve.Solver
import it.unibo.tuprolog.theory.parsing.ClausesParser

fun main() {
    prolog {
        val uri = "redis://localhost:9851"
        val region = RegionFactory.getRegion("test", RegionFactoryParam.BoundParam(
            GeoLocation(-1186.0, 41.0),
            GeoLocation(-1181.0, 45.0)
        )) as Bounds

        val region2 = RegionFactory.getRegion("test", RegionFactoryParam.BoundParam(
            GeoLocation(30.4, 15.0),
            GeoLocation(20.0, 13.0)
        )) as Bounds

        val solver = Solver.spatialSolverWithDefaultBuiltins()
        println(solver.staticKb)
        println(solver.dynamicKb)

        val map = mapOf<String, Region>(Pair("Region", region))
        val map2 = mapOf<String, Region>(Pair("Region", region2))

        Tile38Connection.dropAll(uri)
        val clause = ClausesParser.withDefaultOperators.parseClauses("hello(X) :- X = world.")[0]
        Tile38Connection.putClauseFirst(clause, region, uri)

        val query = "hello"("X")

        when (val it = solver.solveOnce(query, SolveOptions.DEFAULT.setOptions(map2))) {
            is Solution.No -> println("no.\n")
            is Solution.Yes -> {
                println("yes: ${it.solvedQuery}")
                for (assignment in it.substitution) {
                    println("\t${assignment.key} / ${assignment.value}")
                }
                println()
            }
            is Solution.Halt -> {
                println("halt: ${it.exception.message}")
                for (err in it.exception.prologStackTrace) {
                    println("\t $err")
                }
            }
        }

        when (val it = solver.solveOnce(query, SolveOptions.DEFAULT.setOptions(map))) {
            is Solution.No -> println("no.\n")
            is Solution.Yes -> {
                println("yes: ${it.solvedQuery}")
                for (assignment in it.substitution) {
                    println("\t${assignment.key} / ${assignment.value}")
                }
                println()
            }
            is Solution.Halt -> {
                println("halt: ${it.exception.message}")
                for (err in it.exception.prologStackTrace) {
                    println("\t $err")
                }
            }
        }
    }
}
