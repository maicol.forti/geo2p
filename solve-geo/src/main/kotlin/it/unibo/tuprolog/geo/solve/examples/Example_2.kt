package it.unibo.tuprolog.geo.solve.examples

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.Var
import it.unibo.tuprolog.unify.Unificator


fun main() {
    /*
    val unificator = Unificator.default
    val cached = Unificator.cached(unificator, capacity = 5)

    val term = Struct.of("father", Atom.of("abraham"), Atom.of("isaac"))
    val template = Struct.of("father", Var.of("X"), Atom.of("isaac"))

    val substitution: Substitution = cached.mgu(term, template)
    val match: Boolean = cached.match(term, template)
    val unified: Term? = cached.unify(term, template)

    println(substitution) // {X_0=abraham}
    println(match) // true
    println(unified) // father(abraham, isaac)
    */
    /*Scope.empty {
        val unificator = object : AbstractUnificator() {
            override fun checkTermsEquality(first: Term, second: Term): Boolean = when {
                first is Integer && second is Integer ->
                    first.value.absoluteValue.compareTo(second.value.absoluteValue) == 0
                first is Numeric && second is Numeric ->
                    first.decimalValue.absoluteValue.compareTo(second.decimalValue.absoluteValue) == 0
                else -> first == second
            }
        }

        val term1 = structOf("f", numOf("1"))
        val term2 = structOf("f", numOf("-1"))

        val match = unificator.match(term1, term2) // true
        val result = unificator.unify(term1, term2) // f(1)

        println(match)
        println(result)
    }*/
    /*
    val unificator = Unificator.default

    val term = Struct.of("father", Atom.of("abraham"), Atom.of("isaac"))
    val template = Struct.of("father", Atom.of("isaac"), Atom.of("abraham"))

    val substitution: Substitution = unificator.mgu(term, template)
    val match: Boolean = unificator.match(term, template)
    val unified: Term? = unificator.unify(term, template)

    println(substitution is Substitution.Fail) // true
    println(substitution.isFailed) // true
    println(match) // false
    println(unified) // null
*/
    /*
    val term = Struct.of("father", Atom.of("abraham"), Atom.of("isaac"))
    val template = Struct.of("father", Var.of("X"), Atom.of("isaac"))

    println(term mguWith template) // {X_0=abraham}
    println(term matches template) // true
    println(term unifyWith template) // father(abraham, isaac)
*/
    /*prolog {
        Scope.empty {
            val term = "g"("X", "Y")
            val otherTerm = "g"("f"("X"), "a")

            val unificator = Unificator.default

            val mgu = unificator.mgu(term, otherTerm, occurCheckEnabled = true)
            println(mgu) // {X_0=f(X_0), Y_1=a} => WRONG
        }
    }*/
    val unificator = Unificator.default

    val term = Struct.of("father", Atom.of("abraham"), Atom.of("isaac"))
    val template = Struct.of("father", Var.of("X"), Atom.of("isaac"))

    val substitution: Substitution = unificator.mgu(term, template)
    val match: Boolean = unificator.match(term, template)
    val unified: Term? = unificator.unify(term, template)

    println(substitution) // {X_0=abraham}
    println(match) // true
    println(unified) // father(abraham, isaac)

}
