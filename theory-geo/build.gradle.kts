val tuPrologVersion: String by project

dependencies {
    api("it.unibo.tuprolog:theory-jvm:$tuPrologVersion")
    api("it.unibo.tuprolog:parser-theory-jvm:$tuPrologVersion")
    api("io.lettuce", "lettuce-core", "6.0.2.RELEASE")
    api("it.unibo.tuprolog", "serialize-core", tuPrologVersion)
    api("it.unibo.tuprolog", "serialize-theory", tuPrologVersion)
    api("org.json", "json", "20201115")
}