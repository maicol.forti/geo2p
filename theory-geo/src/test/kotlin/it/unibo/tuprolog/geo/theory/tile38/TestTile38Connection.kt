package it.unibo.tuprolog.geo.theory.tile38

import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.core.parsing.parse
import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.region.RegionFactory
import it.unibo.tuprolog.geo.theory.region.RegionFactoryParam
import it.unibo.tuprolog.theory.parsing.ClausesParser
import org.junit.Test
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import kotlin.test.assertTrue

class TestTile38Connection {
    private val uri = "redis://localhost:9851"
    private val bounds = RegionFactory.getRegion("test", RegionFactoryParam.BoundParam(
        GeoLocation(-1186.0, 41.0),
        GeoLocation(-1181.0, 45.0)
    ))
    private val clause = ClausesParser.withDefaultOperators.parseClauses("hello(X) :- X = test.")

    @Test
    fun loadClausesInRegionFromStart() {
        Tile38Connection.dropAll(uri)
        val result = Tile38Connection.loadClausesInRegion(bounds, uri)
        assertTrue(result.isEmpty())
    }

    @Test
    fun putClauseFirstAndLoad() {
        Tile38Connection.dropAll(uri)
        val clause = ClausesParser.withDefaultOperators.parseClauses("hello(X) :- X = test.")
        Tile38Connection.putClauseFirst(clause[0], bounds, uri)
        val result = Tile38Connection.loadClausesInRegion(bounds, uri)
        assertTrue(result[0].structurallyEquals(clause[0]))
    }

    @Test
    fun putClauseLastAndLoad() {
        Tile38Connection.dropAll(uri)
        val clause = ClausesParser.withDefaultOperators.parseClauses("hello(X) :- X = test.")
        Tile38Connection.putClauseLast(clause[0], bounds, uri)
        val result = Tile38Connection.loadClausesInRegion(bounds, uri)
        assertTrue(result[0].structurallyEquals(clause[0]))
    }

    @Test
    fun retractClause() {
        Tile38Connection.dropAll(uri)
        val clause = ClausesParser.withDefaultOperators.parseClauses("hello(X) :- X = test.")
        Tile38Connection.putClauseLast(clause[0], bounds, uri)
        Tile38Connection.putClauseFirst(clause[0], bounds, uri)
        Tile38Connection.retractClause(clause[0], bounds, uri)
        val result = Tile38Connection.loadClausesInRegion(bounds, uri)
        assertTrue(result.size == 1)
        Tile38Connection.retractClause(clause[0], bounds, uri)
        val result2 = Tile38Connection.loadClausesInRegion(bounds, uri)
        assertTrue(result2.isEmpty())
    }

    @Test
    fun retractAllClause() {
        Tile38Connection.dropAll(uri)
        val clause = ClausesParser.withDefaultOperators.parseClauses("hello(X) :- X = test.")
        Tile38Connection.putClauseFirst(clause[0], bounds, uri)
        Tile38Connection.putClauseFirst(clause[0], bounds, uri)
        Tile38Connection.retractAllClause(clause[0], bounds, uri)
        val result = Tile38Connection.loadClausesInRegion(bounds, uri)
        println(result)
        assertTrue(result.isEmpty())
    }

    @Test
    fun putClauseFirstSafety() {
        Tile38Connection.dropAll(uri)
        val executor = Executors.newFixedThreadPool(100)
        val tasks = mutableListOf<Callable<Unit>>()
        for (i in 0..9) {
            tasks += Callable { Tile38Connection.putClauseFirst(clause[0], bounds, uri) }
        }
        executor.invokeAll(tasks)
        val result = Tile38Connection.loadClausesInRegion(bounds, uri)
        assertTrue(result.size == 10)

    }

    @Test
    fun putClauseLastSafety() {
        Tile38Connection.dropAll(uri)
        val executor = Executors.newFixedThreadPool(100)
        val tasks = mutableListOf<Callable<Unit>>()
        for (i in 0..9) {
            tasks += Callable { Tile38Connection.putClauseLast(clause[0], bounds, uri) }
        }
        executor.invokeAll(tasks)
        val result = Tile38Connection.loadClausesInRegion(bounds, uri)
        assertTrue(result.size == 10)

    }

    @Test
    fun retractClauseSafety() {
        Tile38Connection.dropAll(uri)

        for (i in 0..9) {
            Tile38Connection.putClauseFirst(clause[0], bounds, uri)
        }

        val executor = Executors.newFixedThreadPool(100)
        val tasks = mutableListOf<Callable<Unit>>()
        for (i in 0..9) {
            tasks += Callable { Tile38Connection.retractClause(clause[0], bounds, uri) }
        }
        executor.invokeAll(tasks)
        val result = Tile38Connection.loadClausesInRegion(bounds, uri)
        assertTrue(result.isEmpty())

    }

    @Test
    fun retractAllClauseSafety() {
        Tile38Connection.dropAll(uri)
        val clause2 = Clause.parse("print(X) :- X = hello.")
        val clause3 = Clause.parse("test :- safety.")

        for (i in 0..4) {
            Tile38Connection.putClauseFirst(clause[0], bounds, uri)
        }
        for (i in 0..4) {
            Tile38Connection.putClauseFirst(clause2, bounds, uri)
        }
        for (i in 0..4) {
            Tile38Connection.putClauseFirst(clause3, bounds, uri)
        }

        val executor = Executors.newFixedThreadPool(100)
        val tasks = mutableListOf<Callable<Unit>>()
        tasks.add(Callable { Tile38Connection.retractAllClause(clause[0], bounds, uri) })
        tasks.add(Callable { Tile38Connection.retractAllClause(clause2, bounds, uri) })
        tasks.add(Callable { Tile38Connection.retractAllClause(clause3, bounds, uri) })

        executor.invokeAll(tasks)
        val result = Tile38Connection.loadClausesInRegion(bounds, uri)
        assertTrue(result.isEmpty())
    }

    @Test
    fun putClauseFirstAndLastSafety() {
        Tile38Connection.dropAll(uri)
        val executor = Executors.newFixedThreadPool(100)

        val tasks = mutableListOf<Callable<Unit>>()
        for (i in 0..4) {
            tasks += Callable { Tile38Connection.putClauseFirst(clause[0], bounds, uri) }
        }
        for (i in 0..4) {
            tasks += Callable { Tile38Connection.putClauseLast(clause[0], bounds, uri) }
        }
        executor.invokeAll(tasks)
        val result = Tile38Connection.loadClausesInRegion(bounds, uri)
        assertTrue(result.size == 10)

    }

    @Test
    fun retractAndPutSafety() {
        Tile38Connection.dropAll(uri)
        val clause2 = Clause.parse("print(X) :- X = hello.")
        val executor = Executors.newFixedThreadPool(100)
        for (i in 0..9) {
         Tile38Connection.putClauseFirst(clause[0], bounds, uri)
        }

        val tasks = mutableListOf<Callable<Unit>>()
        for (i in 0..4) {
            tasks += Callable { Tile38Connection.putClauseFirst(clause2, bounds, uri) }
        }
        for (i in 0..4) {
            tasks += Callable { Tile38Connection.retractClause(clause[0], bounds, uri) }
        }
        executor.invokeAll(tasks)
        val result = Tile38Connection.loadClausesInRegion(bounds, uri)
        assertTrue(result.size == 10)
    }

}