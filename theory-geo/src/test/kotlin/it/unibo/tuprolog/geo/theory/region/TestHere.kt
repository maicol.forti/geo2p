package it.unibo.tuprolog.geo.theory.region

import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.geoshape.impl.GeoPoint
import it.unibo.tuprolog.geo.theory.region.impl.Here
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestHere{
    private val id = "test"
    private val location = GeoLocation(-1183.0, 43.0)
    private val radius = 1000

    private val region: Region = RegionFactory.getRegion("test", RegionFactoryParam.HereParam(
        location,
        radius
    ))

    @Test
    fun hereCreation() {
        assertTrue(region is Here)
        assertEquals(region.type, RegionType.HERE)
        assertEquals(region.id, id)
        assertEquals(region.getLocation(), location)
        assertEquals(region.getRadius(), radius)
    }

    @Test
    fun regionShape() {
        val geoShape = GeoPoint(
            id,
            location
        )
        assertEquals(region.shape.id, region.id)
        assertEquals(region.shape.coordinates, geoShape.coordinates)
    }
}
