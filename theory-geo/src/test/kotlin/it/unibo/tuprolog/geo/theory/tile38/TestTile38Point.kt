package it.unibo.tuprolog.geo.theory.tile38

import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.core.parsing.parse
import it.unibo.tuprolog.geo.theory.tile38.tile38object.impl.Tile38Point
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestTile38Point {

    private val clause = Clause.parse("hello(X) :- X = world.")
    private val clause2 = Clause.parse("print(X) :- X = hello.")
    private val tile38Object = Tile38Point("test", "[-1183.0, 43.0]", listOf(clause))
    @Test
    fun addClauseFirst(){
        val addClauseFirst = tile38Object.addClauseFirst(clause2)
        assertTrue(addClauseFirst.clauses[0].structurallyEquals(clause2))
        assertTrue(addClauseFirst.clauses[1].structurallyEquals(clause))
    }

    @Test
    fun addClauseLast(){
        val addClauseLast = tile38Object.addClauseLast(clause2)
        assertTrue(addClauseLast.clauses[1].structurallyEquals(clause2))
        assertTrue(addClauseLast.clauses[0].structurallyEquals(clause))
    }

    @Test
    fun retractClause(){
        val tryRetractClause = tile38Object.retractClause(clause2)
        assertTrue(tryRetractClause.clauses[0].structurallyEquals(clause))
        val retractClause = tile38Object.retractClause(clause)
        assertTrue(retractClause.clauses.isEmpty())
    }

    @Test
    fun conversion(){
        val result =  "{\"type\": \"Point\", \"coordinates\": ${tile38Object.coordinates}, \"Prolog\": \"${tile38Object.clausesString}\"}"
        assertEquals(tile38Object.convertToGeoJSON(), result)
    }

}