package it.unibo.tuprolog.geo.theory.geoshape

import it.unibo.tuprolog.geo.theory.geoshape.impl.GeoPoint
import it.unibo.tuprolog.geo.theory.tile38.tile38object.impl.Tile38Point
import it.unibo.tuprolog.theory.parsing.ClausesParser
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestGeoPoint{
    private val id = "test"

    private val clauses =  ClausesParser.withDefaultOperators.parseTheory(
        """
        hello(X) :- X = world.
        """.trimIndent()
    ).clauses.toList()

    @Test
    fun convertIntoTile38Point() {
        val geoShape = GeoPoint(
            id,
            GeoLocation(-1183.0, 43.0)
        )

        val tile38Object = geoShape.convertToTile38Object(clauses)

        assertTrue(tile38Object is Tile38Point)
        assertEquals(tile38Object.id, id)
        assertEquals(tile38Object.clauses, clauses)
        assertEquals(tile38Object.coordinates, geoShape.coordinates)
    }

}