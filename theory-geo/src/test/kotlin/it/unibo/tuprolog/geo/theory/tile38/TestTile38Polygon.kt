package it.unibo.tuprolog.geo.theory.tile38

import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.core.parsing.parse
import it.unibo.tuprolog.geo.theory.tile38.tile38object.impl.Tile38Polygon
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestTile38Polygon {

    private val clause = Clause.parse("hello(X) :- X = world.")
    private val clause2 = Clause.parse("print(X) :- X = hello.")
    private val coordinates = "[[[-1186.0, 41.0],[-1181.0, 41.0],[-1181.0, 45.0],[-1186.0, 45.0],[-1186.0, 41.0]]]"
    private val tile38Object = Tile38Polygon(
        "test",
        coordinates,
        listOf(clause))

    @Test
    fun addClauseFirst(){
        val addClauseFirst = tile38Object.addClauseFirst(clause2)
        assertTrue(addClauseFirst.clauses[0].structurallyEquals(clause2))
        assertTrue(addClauseFirst.clauses[1].structurallyEquals(clause))
    }

    @Test
    fun addClauseLast(){
        val addClauseLast = tile38Object.addClauseLast(clause2)
        assertTrue(addClauseLast.clauses[1].structurallyEquals(clause2))
        assertTrue(addClauseLast.clauses[0].structurallyEquals(clause))
    }

    @Test
    fun retractClause(){
        val tryRetractClause = tile38Object.retractClause(clause2)
        assertTrue(tryRetractClause.clauses[0].structurallyEquals(clause))
        val retractClause = tryRetractClause.retractClause(clause)
        assertTrue(retractClause.clauses.isEmpty())
        val emptyRetract = retractClause.retractClause(clause)
        assertTrue(emptyRetract.clauses.isEmpty())
    }

    @Test
    fun conversion(){
        val result =  "{\"type\": \"Polygon\", \"coordinates\": ${tile38Object.coordinates}, \"Prolog\": \"${tile38Object.clausesString}\"}"
        assertEquals(tile38Object.convertToGeoJSON(), result)
    }
}