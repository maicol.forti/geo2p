package it.unibo.tuprolog.geo.theory.geoshape

import it.unibo.tuprolog.geo.theory.geoshape.impl.GeoRectangle
import it.unibo.tuprolog.geo.theory.tile38.tile38object.impl.Tile38Polygon
import it.unibo.tuprolog.theory.parsing.ClausesParser
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestGeoRectangle{
    private val id = "test"

    private val clauses =  ClausesParser.withDefaultOperators.parseTheory(
        """
        hello(X) :- X = world.
        """.trimIndent()
    ).clauses.toList()

    @Test
    fun convertIntoTile38Polygon() {
        val geoShape = GeoRectangle(
            "test",
            GeoLocation(-1186.0, 41.0),
            GeoLocation(-1181.0, 41.0),
            GeoLocation(-1181.0, 45.0),
            GeoLocation(-1186.0, 45.0)
        )

        val tile38Object = geoShape.convertToTile38Object(clauses)

        assertTrue(tile38Object is Tile38Polygon)
        assertEquals(tile38Object.id, id)
        assertEquals(tile38Object.clauses, clauses)
        assertEquals(tile38Object.coordinates, geoShape.coordinates)
    }

}