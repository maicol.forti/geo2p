package it.unibo.tuprolog.geo.theory.theory

import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.region.RegionFactory
import it.unibo.tuprolog.geo.theory.region.RegionFactoryParam
import it.unibo.tuprolog.geo.theory.region.impl.Bounds
import it.unibo.tuprolog.geo.theory.theory.impl.Tile38MutableSpatialTheory
import it.unibo.tuprolog.geo.theory.theory.impl.Tile38SpatialTheory
import it.unibo.tuprolog.geo.theory.tile38.Tile38Connection
import it.unibo.tuprolog.theory.parsing.ClausesParser
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestAssertA {
    private val uri = "redis://localhost:9851"
    private val clause =  ClausesParser.withDefaultOperators.parseTheory(
        """
        hello(X) :- X = world.
        """.trimIndent()
    ).clauses.toList()[0]

    private val clause2 =  ClausesParser.withDefaultOperators.parseTheory(
        """
        print(X) :- X = hello.
        """.trimIndent()
    ).clauses.toList()[0]

    private val bounds = RegionFactory.getRegion("test", RegionFactoryParam.BoundParam(
        GeoLocation(-1186.0, 41.0),
        GeoLocation(-1181.0, 45.0)
    )) as Bounds

    @Test
    fun assertA_immutable() {
        Tile38Connection.dropAll(uri)

        val theory = Tile38SpatialTheory(
            bounds,
            mapOf(),
            uri
        )
        assertEquals(theory.clauses.toList(), listOf())
        val newTheory = theory.assertA(clause)
        val assertClause = newTheory.clauses.toList()[0]

        assertEquals(listOf(), theory.clauses.toList())
        assertTrue(assertClause.structurallyEquals(clause))

        val assertAgainTheory = newTheory.assertA(clause2)

        val firstClause = assertAgainTheory.clauses.toList()[0]
        val secondClause = assertAgainTheory.clauses.toList()[1]

        assertTrue(firstClause.structurallyEquals(clause2))
        assertTrue(secondClause.structurallyEquals(clause))
    }

    @Test
    fun assertA_mutable() {
        Tile38Connection.dropAll(uri)

        val theory = Tile38MutableSpatialTheory(
            bounds,
            mapOf(),
            uri
        )

        assertEquals(listOf(), theory.clauses.toList())

        theory.assertA(clause)

        val assertedClause = theory.clauses.toList()[0]
        assertTrue(assertedClause.structurallyEquals(clause))

        theory.assertA(clause2)
        val newAssertedClause = theory.clauses.toList()[0]
        val secondAssertedClause = theory.clauses.toList()[1]

        assertTrue(newAssertedClause.structurallyEquals(clause2))
        assertTrue(secondAssertedClause.structurallyEquals(clause))
    }

   

}