package it.unibo.tuprolog.geo.theory.region

import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.geoshape.impl.GeoRectangle
import it.unibo.tuprolog.geo.theory.region.impl.Bounds
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class TestBounds{
    private val id = "test"
    private val southWesternPoint = GeoLocation(-1186.0, 41.0)
    private val northEasternPoint = GeoLocation(-1181.0, 45.0)
    private val region = RegionFactory.getRegion(id, RegionFactoryParam.BoundParam(
        southWesternPoint,
        northEasternPoint
    ))

    @Test
    fun boundsCreation() {
        assertTrue(region is Bounds)
        assertEquals(region.type, RegionType.BOUNDS)
        assertEquals(region.id, id)
        assertEquals(region.getSouthWestern(), southWesternPoint)
        assertEquals(region.getNorthEastern(), northEasternPoint)
        assertFalse(region.within)
    }

    @Test
    fun regionShape() {
        val geoShape = GeoRectangle(
            id,
            southWesternPoint,
            GeoLocation(
                northEasternPoint.longitude,
                southWesternPoint.latitude
            ),
            northEasternPoint,
            GeoLocation(
                southWesternPoint.longitude,
                northEasternPoint.latitude
            )
        )
        assertEquals(region.shape.id, region.id)
        assertEquals(region.shape.coordinates, geoShape.coordinates)
    }
}
