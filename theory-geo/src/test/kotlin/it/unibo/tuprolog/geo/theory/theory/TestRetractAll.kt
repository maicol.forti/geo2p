package it.unibo.tuprolog.geo.theory.theory

import it.unibo.tuprolog.geo.theory.region.impl.Bounds
import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.region.RegionFactory
import it.unibo.tuprolog.geo.theory.region.RegionFactoryParam
import it.unibo.tuprolog.geo.theory.theory.impl.Tile38MutableSpatialTheory
import it.unibo.tuprolog.geo.theory.theory.impl.Tile38SpatialTheory
import it.unibo.tuprolog.geo.theory.tile38.Tile38Connection
import it.unibo.tuprolog.theory.RetractResult
import it.unibo.tuprolog.theory.parsing.ClausesParser
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestRetractAll {
    private val uri = "redis://localhost:9851"
    private val clause =  ClausesParser.withDefaultOperators.parseTheory(
        """
        hello(X) :- X = world.
        """.trimIndent()
    ).clauses.toList()[0]

    private val clause2 =  ClausesParser.withDefaultOperators.parseTheory(
        """
        print(X) :- X = hello.
        """.trimIndent()
    ).clauses.toList()[0]

    val bounds = RegionFactory.getRegion(
        "test",
        RegionFactoryParam.BoundParam(
            GeoLocation(-1186.0, 41.0),
            GeoLocation(-1181.0, 45.0)
        )) as Bounds

    @Test
    fun retractAll_immutable() {
        Tile38Connection.dropAll(uri)

        val theory = Tile38SpatialTheory(
            bounds,
            mapOf(),
            uri
        )

        assertEquals(theory.clauses.toList(), listOf())
        val newTheory = theory.assertA(clause)
        val assertAgainTheory = newTheory.assertA(clause2)
        val lastAssertTheory = assertAgainTheory.assertA(clause)
        val retract = lastAssertTheory.retractAll(clause) as RetractResult.Success

        assertEquals(2, retract.clauses.toList().size)
        assertTrue(retract.firstClause.structurallyEquals(clause))
        assertTrue(retract.clauses.toList()[1].structurallyEquals(clause))
        assertEquals(1, retract.theory.size)
        assertTrue(retract.theory.clauses.toList()[0].structurallyEquals(clause2))
    }

    @Test
    fun retractAll_mutable() {
        Tile38Connection.dropAll(uri)

        val theory = Tile38MutableSpatialTheory(
            bounds,
            mapOf(),
            uri
        )

        theory.assertA(clause)
        theory.assertA(clause2)
        theory.assertA(clause2)
        val retract = theory.retractAll(clause2) as RetractResult.Success

        assertEquals(2, retract.clauses.toList().size)
        assertTrue(retract.firstClause.structurallyEquals(clause2))
        assertTrue(retract.clauses.toList()[1].structurallyEquals(clause2))
        assertEquals(1, retract.theory.size)
        assertTrue(retract.theory.clauses.toList()[0].structurallyEquals(clause))
    }

    

}