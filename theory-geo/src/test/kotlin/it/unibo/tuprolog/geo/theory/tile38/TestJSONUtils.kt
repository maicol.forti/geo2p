package it.unibo.tuprolog.geo.theory.tile38

import io.lettuce.core.RedisClient
import io.lettuce.core.dynamic.RedisCommandFactory
import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.core.parsing.parse
import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.region.RegionFactory
import it.unibo.tuprolog.geo.theory.region.RegionFactoryParam
import it.unibo.tuprolog.geo.theory.region.impl.Bounds
import org.junit.Test
import kotlin.test.assertTrue

class TestJSONUtils {
    private val uri = "redis://localhost:9851"
    private val bound = RegionFactory.getRegion("bound", RegionFactoryParam.BoundParam(
        GeoLocation(-1184.3, 42.6),
        GeoLocation(-1182.9, 43.6)
    ))

    private val bound2 = RegionFactory.getRegion("bound2", RegionFactoryParam.BoundParam(
        GeoLocation(-1182.7, 44.3),
        GeoLocation(-1182.1, 44.7)
    ))

    private val bound3 = RegionFactory.getRegion("bound3", RegionFactoryParam.BoundParam(
        GeoLocation(-1185.7, 44.3),
        GeoLocation(-1183.1, 44.7)
    ))

    private val boundSearch = RegionFactory.getRegion("test",RegionFactoryParam.BoundParam(
        GeoLocation(-1186.0, 41.0),
        GeoLocation(-1181.0, 45.0)
    )) as Bounds


    @Test
    fun sortGeoJSONArray() {
        val client = RedisClient.create(uri)
        val connection = client.connect()
        val factory = RedisCommandFactory(connection)
        val commands = factory.getCommands(Tile38Commands::class.java)

        val clause = Clause.parse("hello(X) :- X = world.")

        val geoObject = bound.shape
        val geoJSON = geoObject.convertToTile38Object(listOf(clause)).convertToGeoJSON()
        val geoObject2 = bound2.shape
        val geoJSON2 = geoObject2.convertToTile38Object(listOf(clause)).convertToGeoJSON()
        val geoObject3 = bound3.shape
        val geoJSON3 = geoObject3.convertToTile38Object(listOf(clause)).convertToGeoJSON()

        commands.drop()
        commands.set("second", geoJSON = geoJSON)
        commands.set("third", geoJSON = geoJSON2)
        commands.set("first", geoJSON = geoJSON3)

        val result = commands.intersects(
            "region",
            boundSearch.getSouthWestern().latitude,
            boundSearch.getSouthWestern().longitude,
            boundSearch.getNorthEastern().latitude,
            boundSearch.getNorthEastern().longitude
        )

        println(result)
        val parsedResult = Tile38Parser.parseResult(result)

        assertTrue(parsedResult[0].id == "first")
        assertTrue(parsedResult[1].id == "second")
        assertTrue(parsedResult[2].id == "third")
    }

}