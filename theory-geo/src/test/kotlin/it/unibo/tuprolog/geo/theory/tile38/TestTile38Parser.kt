package it.unibo.tuprolog.geo.theory.tile38

import io.lettuce.core.RedisClient
import io.lettuce.core.dynamic.RedisCommandFactory
import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.region.RegionFactory
import it.unibo.tuprolog.geo.theory.region.RegionFactoryParam
import it.unibo.tuprolog.geo.theory.region.impl.Bounds
import it.unibo.tuprolog.theory.parsing.ClausesParser
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestTile38Parser {

    private val uri = "redis://localhost:9851"
    private val bound = RegionFactory.getRegion("test", RegionFactoryParam.BoundParam(
        GeoLocation(-1186.2, 41.3),
        GeoLocation(-1181.3, 45.5)
    )) as Bounds
    private val clause = ClausesParser.withDefaultOperators.parseClauses("hello(X) :- X = test.")

    @Test
    fun parseResult() {
        val client = RedisClient.create(uri)
        val connection = client.connect()
        val factory = RedisCommandFactory(connection)
        val commands = factory.getCommands(Tile38Commands::class.java)

        val geoObject = bound.shape
        val geoJSON = geoObject.convertToTile38Object(listOf(clause[0])).convertToGeoJSON()

        commands.drop()
        commands.set("test", geoJSON = geoJSON)

        val result = commands.intersects(
            "region",
            bound.getSouthWestern().latitude,
            bound.getSouthWestern().longitude,
            bound.getNorthEastern().latitude,
            bound.getNorthEastern().longitude
        )
        val parsedResult = Tile38Parser.parseResult(result)

        assertEquals("test", parsedResult[0].id)
        assertTrue(clause[0].structurallyEquals(parsedResult[0].clauses[0]))

    }

}