package it.unibo.tuprolog.geo.theory.tile38

import org.json.JSONArray
import org.json.JSONObject
import java.util.*

/** A comparator for JSONArray*/
private class GeoJSONComparator: Comparator<JSONArray> {
    override fun compare(a: JSONArray, b: JSONArray) : Int {
        val geoA = a.getJSONObject(1)
        val geoB = b.getJSONObject(1)
        val coordinatesA =
            leftmostLongitudeCoordinate(geoA)
        val coordinatesB =
            leftmostLongitudeCoordinate(geoB)
        return coordinatesA.compareTo(coordinatesB)
    }
}

/** Return the leftmost longitude coordinate*/
private fun leftmostLongitudeCoordinate(geo: JSONObject): Double =
    geo["coordinates"].toString()
        .replace("[","")
        .replace("]", "")
        .split(",")
        .map { it.trim().toDouble() }
        .filterIndexed {index, _ -> index % 2 == 0}
        .toDoubleArray().sorted()[0]

/** Sort a JSONArray with the GeoJSONComparator*/
fun sortGeoJSONArray(geoJsonArray: JSONArray) : List<JSONArray> {
    val myJsonArrayAsList = mutableListOf<JSONArray>()
    for (i in 0 until geoJsonArray.length()) myJsonArrayAsList.add(geoJsonArray.getJSONArray(i))
    Collections.sort(myJsonArrayAsList,
        GeoJSONComparator()
    )
    return myJsonArrayAsList
}