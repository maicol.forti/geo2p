package it.unibo.tuprolog.geo.theory.tile38.tile38object

enum class Tile38ObjectType(val type: String) {
    POINT("Point"),
    POLYGON("Polygon")
}