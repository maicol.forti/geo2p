package it.unibo.tuprolog.geo.theory.geoshape.impl

import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.geoshape.GeoShape
import it.unibo.tuprolog.geo.theory.tile38.tile38object.Tile38Object
import it.unibo.tuprolog.geo.theory.tile38.tile38object.impl.Tile38Point

/** A point*/
class GeoPoint(
    override val id: String,
    location: GeoLocation
) : GeoShape {
    override val coordinates = "[${location.longitude}, ${location.latitude}]"

    override fun convertToTile38Object(clauses: List<Clause>): Tile38Object
        = Tile38Point(id, coordinates, clauses)
}