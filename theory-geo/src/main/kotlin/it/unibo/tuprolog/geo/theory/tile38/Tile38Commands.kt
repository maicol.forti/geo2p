package it.unibo.tuprolog.geo.theory.tile38

import io.lettuce.core.dynamic.Commands
import io.lettuce.core.dynamic.annotation.Command

/** Definitions of possible Tile38 commands*/
interface Tile38Commands: Commands {
    /** Set the value of an id. If a value is already associated to that key/id, it'll be overwritten*/
    @Command("SET region ?0 OBJECT ?1")
    fun set(
        key: String,
        geoJSON: String
    ): List<Any?>?

    /** Searches a collection for objects that intersect a specified bounding area*/
    @Command("INTERSECTS ?0 BOUNDS ?1 ?2 ?3 ?4")
    fun intersects(
        region: String,
        southWesternPointLat: Double,
        southWesternPointLong: Double,
        northEasternPointLat: Double,
        northEasternPointLong: Double
    ): List<Any?>?

    /** Searches a collection for objects that are within a specified bounding area*/
    @Command("WITHIN ?0 BOUNDS ?1 ?2 ?3 ?4")
    fun within(
        region: String,
        southWesternPointLat: Double,
        southWesternPointLong: Double,
        northEasternPointLat: Double,
        northEasternPointLong: Double
    ): List<Any?>?

    /** Searches a collection for objects that are close to a specified point*/
    @Command("NEARBY ?0 POINT ?1 ?2 ?3")
    fun nearby(
        region: String,
        latitude: Double,
        longitude: Double,
        radius: Int
    ): List<Any?>?

    /** Remove all objects from specified key*/
    @Command("DROP region")
    fun drop(): List<Any?>?
}