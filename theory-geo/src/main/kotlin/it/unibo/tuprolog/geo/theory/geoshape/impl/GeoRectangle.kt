package it.unibo.tuprolog.geo.theory.geoshape.impl

import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.geoshape.GeoPolygon
import it.unibo.tuprolog.geo.theory.tile38.tile38object.Tile38Object
import it.unibo.tuprolog.geo.theory.tile38.tile38object.impl.Tile38Polygon

/** A rectangle*/
class GeoRectangle(
    override val id: String,
    southWesternPoint: GeoLocation,
    southEasternPoint: GeoLocation,
    northEasternPoint: GeoLocation,
    northWesternPoint: GeoLocation
) : GeoPolygon {

    private val swPoint = "[${southWesternPoint.longitude}, ${southWesternPoint.latitude}]"
    private val sePoint = "[${southEasternPoint.longitude}, ${southEasternPoint.latitude}]"
    private val nePoint = "[${northEasternPoint.longitude}, ${northEasternPoint.latitude}]"
    private val nwPoint = "[${northWesternPoint.longitude}, ${northWesternPoint.latitude}]"
    override val coordinates = "[[$swPoint,$sePoint,$nePoint,$nwPoint,$swPoint]]"

    override fun convertToTile38Object(clauses: List<Clause>): Tile38Object
        = Tile38Polygon(id, coordinates, clauses)
}