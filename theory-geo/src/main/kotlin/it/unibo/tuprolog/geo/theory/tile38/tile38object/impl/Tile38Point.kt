package it.unibo.tuprolog.geo.theory.tile38.tile38object.impl

import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.geo.theory.tile38.tile38object.Tile38Object
import it.unibo.tuprolog.utils.addFirst

/** A point that can be stored in Tile38*/
class Tile38Point(
    override val id: String,
    override val coordinates: String,
    override val clauses: List<Clause>
) : Tile38Object {

    override fun convertToGeoJSON(): String {
        return "{\"type\": \"Point\", \"coordinates\": $coordinates, \"$key\": \"${clausesString}\"}"
    }
    override fun addClauseFirst(clause: Clause): Tile38Point {
        val list = clauses.toMutableList()
        list.addFirst(clause)
        return Tile38Point(id, coordinates, list)
    }

    override fun addClauseLast(clause: Clause): Tile38Point {
        val list = clauses.toMutableList()
        list.add(clause)
        return Tile38Point(id, coordinates, list)
    }

    override fun retractClause(clause: Clause): Tile38Object {
        val list = clauses.toMutableList()
        val index = list.indexOfFirst { it.structurallyEquals(clause)}
        if (index != -1) {
            list.removeAt(index)
        }
        return Tile38Point(id, coordinates, list)
    }
}