package it.unibo.tuprolog.geo.theory.tile38

import io.lettuce.core.RedisClient
import io.lettuce.core.dynamic.RedisCommandFactory
import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.geo.theory.region.impl.Bounds
import it.unibo.tuprolog.geo.theory.region.impl.Here
import it.unibo.tuprolog.geo.theory.region.Region
import it.unibo.tuprolog.geo.theory.region.RegionType
import it.unibo.tuprolog.geo.theory.tile38.tile38object.Tile38Object
import it.unibo.tuprolog.theory.parsing.ClausesParser

/** A Tile38Connection*/
object Tile38Connection {
    private const val regionName = "region"

    @Volatile private var currentUri: String? = null
    @Volatile private var client: RedisClient? = null
    @Volatile private var commands: Tile38Commands? = null

    @Synchronized private fun setConnection(uri: String) {
            if (client != null) client?.shutdown()
            client = RedisClient.create(uri)
            val connection = client?.connect()
            val factory = RedisCommandFactory(connection)
            commands = factory.getCommands(Tile38Commands::class.java)
            currentUri = uri
    }

    @Synchronized private fun setConnectionIfNecessary(uri: String) =
        if (uri != currentUri) setConnection(uri) else Unit
    
    /** Search the given region for clauses, it depends on the region type*/
    @Synchronized private fun searchRegion(region: Region, uri: String) =
        Tile38Parser.parseResult(
            when (region.type) {
                RegionType.BOUNDS -> {
                    if ((region as Bounds).within)
                        within(region, uri)
                    else
                        intersects(region, uri)
                }
                RegionType.HERE -> nearby(
                    region as Here,
                    uri
                )
            }
        )

    /** Searches a collection for objects that intersect a specified bounding area*/
    @Synchronized private fun intersects(bound: Bounds, uri: String): List<Any?>? {
        setConnectionIfNecessary(uri)
        return commands?.intersects(
            regionName,
            bound.getSouthWestern().latitude,
            bound.getSouthWestern().longitude,
            bound.getNorthEastern().latitude,
            bound.getNorthEastern().longitude
        )
    }

    /** Searches a collection for objects that are within a specified bounding area*/
    @Synchronized private fun within(bound: Bounds, uri: String): List<Any?>? {
        setConnectionIfNecessary(uri)
        return commands?.within(
            regionName,
            bound.getSouthWestern().latitude,
            bound.getSouthWestern().longitude,
            bound.getNorthEastern().latitude,
            bound.getNorthEastern().longitude
        )
    }

    /** Searches a collection for objects that are close to a specified point*/
    @Synchronized private fun nearby(here: Here, uri: String): List<Any?>? {
        setConnectionIfNecessary(uri)
        return commands?.nearby(here.id, here.getLocation().latitude, here.getLocation().longitude, here.getRadius())
    }

    /** load all clauses of a region, the search method depends on the region type*/
    @Synchronized fun loadClausesInRegion(region: Region, uri: String) : List<Clause> {
        val result =
            searchRegion(region, uri)
        return ClausesParser.withDefaultOperators.parseClauses(
            prepareClauses(
                result
            )
        )
    }

    /** returns a string of clauses ready to be parsed*/
    private fun prepareClauses(result: List<Tile38Object>) : String {
        val listClauses = result.map { it.clausesString }
        return listClauses.joinToString(separator = "")
    }

    @Synchronized private fun set(id: String, geoJSON: String, uri: String) {
        setConnectionIfNecessary(uri)
        commands?.set(id, geoJSON)
    }

    /** puts the clause first, if no information are geolocated in this area, geolocate the region and the clause in it*/
    @Synchronized fun putClauseFirst(clause: Clause, region: Region, uri: String) {
        val geoLocatedData =
            searchRegion(region, uri)
        if (geoLocatedData.isEmpty()) {
            val tile38Object = region.shape.convertToTile38Object(listOf(clause))
            set(region.id, tile38Object.convertToGeoJSON(), uri)
        } else {
            val newObject = geoLocatedData.first().addClauseFirst(clause)
            set(newObject.id, newObject.convertToGeoJSON(), uri)
        }
    }

    /** puts the clause last, if no information are geolocated in this area, geolocate the region and the clause in it*/
    @Synchronized fun putClauseLast(clause: Clause, region: Region, uri: String) {
        val geoLocatedData =
            searchRegion(region, uri)
        if (geoLocatedData.isEmpty()) {
            val tile38Object = region.shape.convertToTile38Object(listOf(clause))
            set(region.id, tile38Object.convertToGeoJSON(), uri)
        } else {
            val newObject = geoLocatedData.last().addClauseLast(clause)
            set(newObject.id, newObject.convertToGeoJSON(), uri)
        }
    }

    /** retract a matching clause from the region*/
    @Synchronized fun retractClause(clause: Clause, region: Region, uri: String) =
        retractClauses(
            clause,
            region,
            uri,
            false
        )

    /** retract all matching clauses from the region*/
    @Synchronized fun retractAllClause(clause: Clause, region: Region, uri: String) =
        retractClauses(
            clause,
            region,
            uri,
            true
        )

    @Synchronized private fun retractClauses(clause: Clause, region: Region, uri: String, all: Boolean) {
        val geoLocatedData =
            searchRegion(region, uri)
        var retractOne = false
        for (value in geoLocatedData) {
            var tile38Object = value
            if (retractOne) break
            for (clauseValue in value.clauses) {
                if (clauseValue.structurallyEquals(clause)) {
                    tile38Object = tile38Object.retractClause(clauseValue)
                    if (!all) {
                        retractOne = true
                        break
                    }
                }
            }
            set(tile38Object.id, tile38Object.convertToGeoJSON(), uri)
        }
    }

    /** Remove all objects from specified key*/
    @Synchronized fun dropAll(uri: String) {
        setConnectionIfNecessary(uri)
        commands?.drop()
    }
}
