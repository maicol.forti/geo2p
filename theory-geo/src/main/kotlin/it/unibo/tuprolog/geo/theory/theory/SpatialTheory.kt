package it.unibo.tuprolog.geo.theory.theory

import it.unibo.tuprolog.geo.theory.region.Region
import it.unibo.tuprolog.theory.Theory

/** A Theory obtained from a certain region*/
interface SpatialTheory : Theory {

    /** the region*/
    val region: Region
    /** return a SpatialTheory of the specified region.*/
    fun setRegion(region: Region) : SpatialTheory
}