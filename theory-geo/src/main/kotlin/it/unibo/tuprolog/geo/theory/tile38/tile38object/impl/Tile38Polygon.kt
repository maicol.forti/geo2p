package it.unibo.tuprolog.geo.theory.tile38.tile38object.impl

import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.geo.theory.tile38.tile38object.Tile38Object
import it.unibo.tuprolog.utils.addFirst

/** A polygon that can be stored in Tile38*/
class Tile38Polygon(
    override val id: String,
    override val coordinates: String,
    override val clauses: List<Clause>
) : Tile38Object {

    override fun convertToGeoJSON(): String {
        return "{\"type\": \"Polygon\", \"coordinates\": $coordinates, \"$key\": \"${clausesString}\"}"
    }

    override fun addClauseFirst(clause: Clause): Tile38Polygon {
        val list = clauses.toMutableList()
        list.addFirst(clause)
        return Tile38Polygon(id, coordinates, list)
    }

    override fun addClauseLast(clause: Clause): Tile38Polygon {
        val list = clauses.toMutableList()
        list.add(clause)
        return Tile38Polygon(id, coordinates, list)
    }

    override fun retractClause(clause: Clause): Tile38Object {
        val list = clauses.toMutableList()
        list.remove(clause)
        return Tile38Polygon(id, coordinates, list)
    }
}