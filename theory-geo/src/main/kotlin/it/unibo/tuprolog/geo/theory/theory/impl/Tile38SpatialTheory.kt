package it.unibo.tuprolog.geo.theory.theory.impl

import it.unibo.tuprolog.collections.ClauseQueue
import it.unibo.tuprolog.collections.MutableClauseQueue
import it.unibo.tuprolog.collections.RetrieveResult
import it.unibo.tuprolog.core.*
import it.unibo.tuprolog.geo.theory.region.Region
import it.unibo.tuprolog.geo.theory.theory.SpatialTheory
import it.unibo.tuprolog.geo.theory.tile38.Tile38Connection
import it.unibo.tuprolog.theory.MutableTheory
import it.unibo.tuprolog.theory.RetractResult
import it.unibo.tuprolog.theory.Theory
import it.unibo.tuprolog.utils.dequeOf

/** An implementation of SpatialTheory specific to Tile38, requires a region and a Redis URI.*/
class Tile38SpatialTheory(
    override val region: Region,
    override val tags: Map<String, Any>,
    private val uri: String
): SpatialTheory {

    private val cache: MutableTheory = MutableTheory.empty()
    private var loaded: Boolean = false

    override fun setRegion(region: Region): SpatialTheory =
        Tile38SpatialTheory(region, tags, uri)

    /** Load clauses and put each one in cache*/
    private fun loadClausesFromTile38() {
        cache.assertA(Tile38Connection.loadClausesInRegion(region, uri))
        loaded = true
    }

    /** Load clauses if not already done*/
    private fun loadClausesFromTile38IfNecessary() =
        if (!loaded) loadClausesFromTile38() else Unit

    /** All [Clause]s in this theory */
    override val clauses: Iterable<Clause> get() {
        loadClausesFromTile38IfNecessary()
        return cache.clauses
    }

    /** The amount of clauses in this [Theory] */
    override val size: Long get() {
        loadClausesFromTile38IfNecessary()
        return cache.size
    }

    /** Adds given clause before all other clauses in this theory */
    override fun assertA(clause: Clause): Theory {
        loadClausesFromTile38IfNecessary()
        Tile38Connection.putClauseFirst(clause, region, uri)
        return Tile38SpatialTheory(region, tags, uri)
    }

    /** Adds the given clauses before all other clauses in this theory */
    override fun assertA(clauses: Iterable<Clause>): Theory {
        clauses.toList().asReversed().forEach{assertA(it)}
        return Tile38SpatialTheory(region, tags, uri)
    }

    /** Adds the given clauses before all other clauses in this theory */
    override fun assertA(clauses: Sequence<Clause>): Theory {
        assertA(clauses.asIterable())
        return Tile38SpatialTheory(region, tags, uri)
    }

    /** Adds given clause after all other clauses in this theory */
    override fun assertZ(clause: Clause): Theory {
        loadClausesFromTile38IfNecessary()
        Tile38Connection.putClauseLast(clause, region, uri)
        return Tile38SpatialTheory(region, tags, uri)
    }

    /** Adds the given clauses after all other clauses in this theory */
    override fun assertZ(clauses: Iterable<Clause>): Theory {
        clauses.toList().forEach{assertZ(it)}
        return Tile38SpatialTheory(region, tags, uri)
    }

    /** Adds the given clauses after all other clauses in this theory */
    override fun assertZ(clauses: Sequence<Clause>): Theory {
        assertZ(clauses.asIterable())
        return Tile38SpatialTheory(region, tags, uri)
    }

    /** Checks if given clause is contained in this theory */
    override fun contains(clause: Clause): Boolean {
        loadClausesFromTile38IfNecessary()
        return get(clause).any()
    }

    /** Checks if clauses exists in this theory having the specified [Indicator] as head; this should be [well-formed][Indicator.isWellFormed] */
    override fun contains(indicator: Indicator): Boolean {
        loadClausesFromTile38IfNecessary()
        return get(indicator).any()
    }

    /** Checks if given clause is present in this theory */
    override fun contains(head: Struct): Boolean {
        loadClausesFromTile38IfNecessary()
        return contains(Rule.of(head, Var.anonymous()))
    }

    /** Retrieves matching clauses from this theory */
    override fun get(clause: Clause): Sequence<Clause> {
        loadClausesFromTile38IfNecessary()
        return cache[clause]
    }

    /** Adds given [Theory] to this */
    override fun plus(theory: Theory): Theory = assertZ(theory)

    /** Tries to delete a matching clause from this theory */
    override fun retract(clause: Clause): RetractResult<Theory> {
        loadClausesFromTile38IfNecessary()
        val newTheory = ClauseQueue.of(clauses)
        return when (val retracted = newTheory.retrieveFirst(clause)) {
            is RetrieveResult.Failure ->
                RetractResult.Failure(this)
            else -> {
                (retracted as RetrieveResult.Success).clauses.forEach{ Tile38Connection.retractClause(it, region, uri)}
                RetractResult.Success(
                    Tile38SpatialTheory(
                        region,
                        tags,
                        uri
                    ),
                    retracted.clauses
                )
            }
        }
    }

    /** Tries to delete the matching clauses from this theory */
    override fun retract(clauses: Iterable<Clause>): RetractResult<Theory> {
        loadClausesFromTile38IfNecessary()
        val newTheory = MutableClauseQueue.of(this.clauses)
        val removed = dequeOf<Clause>()
        for (clause in clauses) {
            val result = newTheory.retrieveFirst(clause)
            if (result is RetrieveResult.Success) {
                removed.addAll(result.clauses)
            }
        }
        return if (removed.isEmpty()) {
            RetractResult.Failure(this)
        } else {
            removed.forEach{ Tile38Connection.retractClause(it, region, uri)}
            RetractResult.Success(
                Tile38SpatialTheory(region, tags, uri),
                removed
            )
        }
    }

    /** Tries to delete the matching clauses from this theory */
    override fun retract(clauses: Sequence<Clause>): RetractResult<Theory> =
        retract(clauses.asIterable())

    /** Tries to delete all matching clauses from this theory */
    override fun retractAll(clause: Clause): RetractResult<Theory> {
        loadClausesFromTile38IfNecessary()
        val newTheory = ClauseQueue.of(clauses)
        return when (val retracted = newTheory.retrieveAll(clause)) {
            is RetrieveResult.Failure -> RetractResult.Failure(this)
            else -> {
                (retracted as RetrieveResult.Success).clauses.forEach{ Tile38Connection.retractAllClause(it, region, uri)}
                RetractResult.Success(
                    Tile38SpatialTheory(
                        region,
                        tags,
                        uri
                    ),
                    retracted.clauses
                )
            }
        }
    }

    override fun equals(other: Theory, useVarCompleteName: Boolean): Boolean {
        loadClausesFromTile38IfNecessary()
        if (this === other) return true

        val i = clauses.iterator()
        val j = other.clauses.iterator()

        while (i.hasNext() && j.hasNext()) {
            if (i.next().equals(j.next(), useVarCompleteName).not()) {
                return false
            }
        }

        return i.hasNext() == j.hasNext()
    }

    /** Retrieves all rules in this theory having the specified [Indicator] as head; this should be [well-formed][Indicator.isWellFormed] */
    override fun get(indicator: Indicator): Sequence<Rule> {
        loadClausesFromTile38IfNecessary()
        require(indicator.isWellFormed) { "The provided indicator is not well formed: $indicator" }

        return get(
            Rule.of(
                Struct.of(indicator.indicatedName!!, (1..indicator.indicatedArity!!).map { Var.anonymous() }),
                Var.anonymous()
            )
        ).map { it as Rule }
    }

    /** Retrieves matching rules from this theory */
    override fun get(head: Struct): Sequence<Rule> {
        loadClausesFromTile38IfNecessary()
        return get(Rule.of(head, Var.anonymous())).map { it as Rule }
    }

    /**
     * Returns an iterator over the elements of this object.
     */
    override fun iterator(): Iterator<Clause> = clauses.iterator()

    override fun replaceTags(tags: Map<String, Any>): Theory =
        if (tags === this.tags) this else Tile38SpatialTheory(
            region,
            tags,
            uri
        )


    override fun clone(): Theory = Tile38SpatialTheory(region, tags, uri)

    override fun toImmutableTheory(): Theory = this

    override fun toMutableTheory(): MutableTheory =
        Tile38MutableSpatialTheory(region, tags, uri)

    /** An enhanced toString that prints the theory in a Prolog program format, if [asPrologText] is `true` */
    override fun toString(asPrologText: Boolean): String {
        loadClausesFromTile38IfNecessary()
        return when (asPrologText) {
            true -> clauses.joinToString(".\n", "", ".\n")
            false -> toString()
        }
    }

    override fun abolish(indicator: Indicator): Theory {
        loadClausesFromTile38IfNecessary()
        require(indicator.isWellFormed) { "The provided indicator is not well formed: $indicator" }

        return retractAll(Struct.template(indicator.indicatedName!!, indicator.indicatedArity!!)).theory
    }
}