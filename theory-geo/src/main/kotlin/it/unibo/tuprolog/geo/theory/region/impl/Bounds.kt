package it.unibo.tuprolog.geo.theory.region.impl

import it.unibo.tuprolog.geo.theory.region.Region
import it.unibo.tuprolog.geo.theory.region.RegionType
import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.geoshape.GeoShape
import it.unibo.tuprolog.geo.theory.geoshape.impl.GeoRectangle

/** A bounding box consists of two points. The first being the southwestern most point, and the second is the northeastern most point*/
class Bounds(private val identifier: String, private val southWesternPoint: GeoLocation, private val northEasternPoint: GeoLocation, val within: Boolean) :
    Region {
    override val type: RegionType
        get() = RegionType.BOUNDS

    override val id: String
        get() = identifier

    override val shape: GeoShape
        get() =
            GeoRectangle(
                identifier,
                southWesternPoint,
                GeoLocation(
                    northEasternPoint.longitude,
                    southWesternPoint.latitude
                ),
                northEasternPoint,
                GeoLocation(
                    southWesternPoint.longitude,
                    northEasternPoint.latitude
                )
            )

    fun getSouthWestern(): GeoLocation {
        return southWesternPoint
    }
    fun getNorthEastern(): GeoLocation {
        return northEasternPoint
    }
}
