package it.unibo.tuprolog.geo.theory.tile38.tile38object

import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.geo.theory.tile38.tile38object.impl.Tile38Point
import it.unibo.tuprolog.geo.theory.tile38.tile38object.impl.Tile38Polygon

object Tile38ObjectFactory {
    fun getObject(type: String, id: String, coordinates: String, clauses: List<Clause>): Tile38Object {
        return when (type) {
            Tile38ObjectType.POINT.type -> Tile38Point(id, coordinates, clauses)
            else -> Tile38Polygon(id, coordinates, clauses)
        }
    }
}