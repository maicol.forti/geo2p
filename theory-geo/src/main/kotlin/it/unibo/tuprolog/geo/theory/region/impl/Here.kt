package it.unibo.tuprolog.geo.theory.region.impl

import it.unibo.tuprolog.geo.theory.region.Region
import it.unibo.tuprolog.geo.theory.region.RegionType
import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.geoshape.GeoShape
import it.unibo.tuprolog.geo.theory.geoshape.impl.GeoPoint

/** A region of type HERE consists of a point and a certain radius.*/
class Here(private val identifier: String, private val location: GeoLocation, private val radius: Int) :
    Region {
    override val type: RegionType
        get() = RegionType.HERE

    override val id: String
        get() = identifier

    override val shape: GeoShape
        get() = GeoPoint(identifier, location)

    fun getLocation(): GeoLocation {
        return location
    }
    fun getRadius(): Int {
        return radius
    }
}