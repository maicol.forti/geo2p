package it.unibo.tuprolog.geo.theory.tile38.tile38object

import it.unibo.tuprolog.core.Clause

/** Represents the concept of an object that can be converted in GeoJSON format so that it can be geolocated.*/
interface Tile38Object{
    /** The id of the object*/
    val id: String
    /** The coordinates of the object*/
    val coordinates: String
    /** The list of clauses associated to the object*/
    val clauses: List<Clause>

    /** A string representation of the clauses that can be parsed by a clause parser*/
    val clausesString: String
        get() = clauses.joinToString (separator = "") { "$it. " }

    /** a common key between Tile38Object to which clausesString are associated when converted in GeoJSON format*/
    val key: String
        get() = "Prolog"

    /** convert the object into a String in GeoJSON format*/
    fun convertToGeoJSON(): String
    /** returns a new Tile38Object with the given clause in the first position of the list of clauses*/
    fun addClauseFirst(clause: Clause): Tile38Object
    /** returns a new Tile38Object with the given clause in the last position of the list of clauses*/
    fun addClauseLast(clause: Clause): Tile38Object
    /** returns a new Tile38Object with the first occurrence of the given clause removed*/
    fun retractClause(clause: Clause): Tile38Object

}