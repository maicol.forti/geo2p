package it.unibo.tuprolog.geo.theory.geoshape

/** A geolocation is a point defined by a longitude and a latitude*/
class GeoLocation(val longitude: Double, val latitude: Double)

