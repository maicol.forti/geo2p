package it.unibo.tuprolog.geo.theory.geoshape

import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.geo.theory.tile38.tile38object.Tile38Object

/** A representation of a shape that represents a region of space*/
interface GeoShape {
    val id: String
    val coordinates: String
    fun convertToTile38Object(clauses: List<Clause>): Tile38Object
}