package it.unibo.tuprolog.geo.theory.theory.impl

import it.unibo.tuprolog.core.*
import it.unibo.tuprolog.geo.theory.region.Region
import it.unibo.tuprolog.geo.theory.theory.MutableSpatialTheory
import it.unibo.tuprolog.geo.theory.tile38.Tile38Connection
import it.unibo.tuprolog.theory.MutableTheory
import it.unibo.tuprolog.theory.RetractResult
import it.unibo.tuprolog.theory.Theory

/** A mutable implementation of SpatialTheory specific to Tile38*/
class Tile38MutableSpatialTheory(
    private var region: Region,
    override val tags: Map<String, Any>,
    private val uri: String
): MutableSpatialTheory {

    private var cache: MutableTheory = MutableTheory.empty()
    private var loaded: Boolean = false

    override fun setRegion(region: Region): MutableSpatialTheory {
        return this.also {
            this.cache = MutableTheory.empty()
            this.region = region
            this.loaded = false
        }
    }

    /** Load clauses and put each one in cache*/
    private fun loadClausesFromTile38() {
        cache.assertA(Tile38Connection.loadClausesInRegion(region, uri))
        loaded = true
    }

    /** Load clauses if not already done*/
    private fun loadClauseFromTile38IfNecessary() =
        if (!loaded) loadClausesFromTile38() else Unit

    /** All [Clause]s in this theory */
    override val clauses: Iterable<Clause> get() {
        loadClauseFromTile38IfNecessary()
        return cache.clauses
    }

    /** The amount of clauses in this [Theory] */
    override val size: Long get() {
        loadClauseFromTile38IfNecessary()
        return cache.size
    }

    /** Adds given clause before all other clauses in this theory */
    override fun assertA(clause: Clause): MutableTheory {
        return this.also {
            loadClauseFromTile38IfNecessary()
            Tile38Connection.putClauseFirst(clause, region, uri)
            cache.assertA(clause)
        }
    }

    /** Adds the given clauses before all other clauses in this theory */
    override fun assertA(clauses: Iterable<Clause>): MutableTheory =
        this.also { clauses.toList().asReversed().forEach{assertA(it)} }

    /** Adds the given clauses before all other clauses in this theory */
    override fun assertA(clauses: Sequence<Clause>): MutableTheory =
        this.also { assertA(clauses.asIterable()) }

    /** Adds given clause after all other clauses in this theory */
    override fun assertZ(clause: Clause): MutableTheory {
        return this.also {
            loadClauseFromTile38IfNecessary()
            Tile38Connection.putClauseLast(clause, region, uri)
            cache.assertZ(clause)
        }
    }

    /** Adds the given clauses after all other clauses in this theory */
    override fun assertZ(clauses: Iterable<Clause>): MutableTheory =
        this.also { clauses.toList().forEach{assertZ(it)} }

    /** Adds the given clauses after all other clauses in this theory */
    override fun assertZ(clauses: Sequence<Clause>): MutableTheory =
        this.also { assertZ(clauses.asIterable()) }


    /** Checks if given clause is contained in this theory */
    override fun contains(clause: Clause): Boolean {
        loadClauseFromTile38IfNecessary()
        return get(clause).any()
    }

    /** Checks if clauses exists in this theory having the specified [Indicator] as head; this should be [well-formed][Indicator.isWellFormed] */
    override fun contains(indicator: Indicator): Boolean {
        loadClauseFromTile38IfNecessary()
        return get(indicator).any()
    }

    /** Checks if given clause is present in this theory */
    override fun contains(head: Struct): Boolean {
        loadClauseFromTile38IfNecessary()
        return contains(Rule.of(head, Var.anonymous()))
    }

    /** Retrieves matching clauses from this theory */
    override fun get(clause: Clause): Sequence<Clause> {
        loadClauseFromTile38IfNecessary()
        return cache[clause]
    }

    /** Adds given [Theory] to this */
    override fun plus(theory: Theory): MutableTheory = assertZ(theory)

    /** Tries to delete a matching clause from this theory */
    override fun retract(clause: Clause): RetractResult<MutableTheory> {
        loadClauseFromTile38IfNecessary()
        val result = cache.retract(clause)
        when (result) {
            is RetractResult.Success -> result.clauses.forEach{ Tile38Connection.retractClause(it, region, uri) }
        }
        return result
    }

    /** Tries to delete the matching clauses from this theory */
    override fun retract(clauses: Iterable<Clause>): RetractResult<MutableTheory> {
        loadClauseFromTile38IfNecessary()
        val result = cache.retract(clauses)
        when (result) {
            is RetractResult.Success -> result.clauses.forEach{ Tile38Connection.retractClause(it, region, uri) }
        }
        return result
    }

    /** Tries to delete the matching clauses from this theory */
    override fun retract(clauses: Sequence<Clause>): RetractResult<MutableTheory> =
        retract(clauses.asIterable())

    /** Tries to delete all matching clauses from this theory */
    override fun retractAll(clause: Clause): RetractResult<MutableTheory> {
        loadClauseFromTile38IfNecessary()
        val result = cache.retractAll(clause)
        when (result) {
            is RetractResult.Success -> result.clauses.forEach{ Tile38Connection.retractAllClause(it, region, uri) }
        }
        return result
    }

    override fun equals(other: Theory, useVarCompleteName: Boolean): Boolean {
        loadClauseFromTile38IfNecessary()
        if (this === other) return true

        val i = clauses.iterator()
        val j = other.clauses.iterator()

        while (i.hasNext() && j.hasNext()) {
            if (i.next().equals(j.next(), useVarCompleteName).not()) {
                return false
            }
        }

        return i.hasNext() == j.hasNext()
    }

    /** Retrieves all rules in this theory having the specified [Indicator] as head; this should be [well-formed][Indicator.isWellFormed] */
    override fun get(indicator: Indicator): Sequence<Rule> {
        loadClauseFromTile38IfNecessary()
        require(indicator.isWellFormed) { "The provided indicator is not well formed: $indicator" }

        return get(
            Rule.of(
                Struct.of(indicator.indicatedName!!, (1..indicator.indicatedArity!!).map { Var.anonymous() }),
                Var.anonymous()
            )
        ).map { it as Rule }
    }

    /** Retrieves matching rules from this theory */
    override fun get(head: Struct): Sequence<Rule> {
        loadClauseFromTile38IfNecessary()
        return get(Rule.of(head, Var.anonymous())).map { it as Rule }
    }

    /**
     * Returns an iterator over the elements of this object.
     */
    override fun iterator(): Iterator<Clause> = clauses.iterator()

    override fun replaceTags(tags: Map<String, Any>): MutableTheory =
        if (tags === this.tags) this else Tile38MutableSpatialTheory(
            region,
            tags,
            uri
        )

    override fun clone(): MutableTheory = Tile38MutableSpatialTheory(region, tags, uri)

    override fun toImmutableTheory(): Theory =
        Tile38SpatialTheory(region, tags, uri)

    override fun toMutableTheory(): MutableTheory = this

    /** An enhanced toString that prints the theory in a Prolog program format, if [asPrologText] is `true` */
    override fun toString(asPrologText: Boolean): String {
        loadClauseFromTile38IfNecessary()
        return when (asPrologText) {
            true -> clauses.joinToString(".\n", "", ".\n")
            false -> toString()
        }
    }

    override fun abolish(indicator: Indicator): MutableTheory {
        loadClauseFromTile38IfNecessary()
        require(indicator.isWellFormed) { "The provided indicator is not well formed: $indicator" }
        return retractAll(Struct.template(indicator.indicatedName!!, indicator.indicatedArity!!)).theory
    }
}