package it.unibo.tuprolog.geo.theory.region

import it.unibo.tuprolog.geo.theory.geoshape.GeoShape

/** An interface representing a Region of space*/
interface Region {

    /** The region type*/
    val type: RegionType

    /** The region id*/
    val id: String

    /** The region geometric shape*/
    val shape: GeoShape
}