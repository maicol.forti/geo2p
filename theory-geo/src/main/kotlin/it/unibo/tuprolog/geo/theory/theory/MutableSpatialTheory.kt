package it.unibo.tuprolog.geo.theory.theory

import it.unibo.tuprolog.geo.theory.region.Region
import it.unibo.tuprolog.theory.MutableTheory

/** A mutable Theory obtained from a certain region*/
interface MutableSpatialTheory : MutableTheory {
    /** set the current region and return the theory*/
    fun setRegion(region: Region) : MutableSpatialTheory
}