package it.unibo.tuprolog.geo.theory.region

/** region types */
enum class RegionType {
    BOUNDS, HERE
}