package it.unibo.tuprolog.geo.theory.examples

import io.lettuce.core.RedisClient
import io.lettuce.core.codec.StringCodec
import io.lettuce.core.output.StatusOutput
import io.lettuce.core.protocol.CommandArgs
import io.lettuce.core.protocol.CommandType
import it.unibo.tuprolog.core.*
/*import it.unibo.tuprolog.serialize.MimeType
import it.unibo.tuprolog.serialize.TermDeserializer
import it.unibo.tuprolog.serialize.TermSerializer*/
import it.unibo.tuprolog.theory.parsing.ClausesParser
import org.json.JSONObject

fun main(args: Array<String>) {
    Scope.empty {

        //Setup Redis connection
        val client = RedisClient.create("redis://localhost:9851")
        val connection = client.connect()
        val sync = connection.sync()
        val codec = StringCodec.UTF8

        //Setup a term to be geolocalized
        val term = structOf("father", atomOf("abraham"), atomOf("isaac"))
        //val serializer: TermSerializer = TermSerializer.of(MimeType.Json)
        //val serializedTerm = serializer.serialize(term)

        //println("Serialized Term: $serializedTerm")

        //Geolocalize a point with a term
        sync.dispatch(
            CommandType.SET,
            StatusOutput(codec),
            CommandArgs(codec).addValues(
                "fleet",
                "truck1",
                "OBJECT",
                "{\"type\": \"Point\", \"coordinates\": [-112.2693, 33.5123], \"Prolog\": \"print :- universe. hello :- world.\"}"
            )
        )

        //Get the geolocalized point with the term
        val resultGet = sync.dispatch(
            CommandType.GET,
            StatusOutput(codec), CommandArgs(codec)
                .add("fleet")
                .add("truck1")
        )

        println("Tile38 Get: $resultGet")

        //Get the term from the get result
        val jsonObject = JSONObject(resultGet.toString())
        println("JSONObject: ${jsonObject["Prolog"]}")

        //Deserialize the json term to prolog term
       // val deserializer: TermDeserializer = TermDeserializer.of(MimeType.Json)
       // val deserializeGet = deserializer.deserialize(jsonObject.getJSONObject("Prolog").toString())
      //  println("Deserialized term: $deserializeGet");
        val clauses = ClausesParser.withDefaultOperators.parseClauses(jsonObject["Prolog"].toString())

        println(clauses)
        //Use the term
        //val template = Struct.of("father", Var.of("X"), Atom.of("isaac"))
       // val unified: Term? = Unificator.default.unify(deserializeGet, template)

       // println(deserializeGet)

        //println("Unified: $unified")

        client.shutdown()

    }
}