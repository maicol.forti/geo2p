package it.unibo.tuprolog.geo.theory.examples

import it.unibo.tuprolog.core.Clause
import it.unibo.tuprolog.core.parsing.parse
import it.unibo.tuprolog.theory.parsing.ClausesParser

fun main() {

    val clause =  ClausesParser.withDefaultOperators.parseTheory(
        """
        hello(X) :- X = world.
        """.trimIndent()
    ).clauses.toList()[0]

   val clause2 =  ClausesParser.withDefaultOperators.parseTheory(
        """
        hello(X) :- X = world.
        """.trimIndent()
    ).clauses.toList()[0]

    val clause3 =  ClausesParser.withDefaultOperators.parseTheory(
        """
        hello(X) :- X = world.
        """.trimIndent()
    ).clauses.toList()[0]


    println(clause)
    println(clause2)
    println(clause3)


    val clause4 = Clause.parse("hello(X) :- X = world.")
    val clause5 = Clause.parse("hello(X) :- X = world.")
    val clause6 = Clause.parse("hello(X) :- X = world.")

    println(clause4)
    println(clause5)
    println(clause6)

}
