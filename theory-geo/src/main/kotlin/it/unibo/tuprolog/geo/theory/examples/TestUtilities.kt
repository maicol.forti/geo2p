package it.unibo.tuprolog.geo.theory.examples

import it.unibo.tuprolog.core.*
import it.unibo.tuprolog.core.parsing.parse
import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation
import it.unibo.tuprolog.geo.theory.region.*
import it.unibo.tuprolog.geo.theory.tile38.Tile38Connection

fun main() {
    Scope.empty {

        //val connection = Tile38Connector.connect("redis://localhost:9851")
        val uri = "redis://localhost:9851"
        Tile38Connection.dropAll(uri)
        val bound = RegionFactory.getRegion("test",RegionFactoryParam.BoundParam(
            GeoLocation(-1186.0, 41.0),
            GeoLocation(-1181.0, 45.0)
        ))

        //connection.putClauseFirst(clause[0], bound)

        val bound2 = RegionFactory.getRegion("bound2", RegionFactoryParam.BoundParam(
            GeoLocation(-1184.3, 42.6),
            GeoLocation(-1182.9, 43.6)
        ))

        val bound3 = RegionFactory.getRegion("bound3", RegionFactoryParam.BoundParam(
            GeoLocation(-1182.7, 44.3),
            GeoLocation(-1182.1, 44.7)
        ))

        //val clause = ClausesParser.withDefaultOperators.parseClauses("hello(X) :- X = lel.")[0]
        //connection.putClauseFirst(clause, bound)

        val intersects1 = Tile38Connection.loadClausesInRegion(bound, uri)
        println(intersects1)

        val clause = Clause.parse("pepe :- ga.")
        val clause2 = Clause.parse("print(X) :- X = hello.")
        val clause4 = Clause.parse("a :- b.")
        val clause5 = Clause.parse("first :- p.")
        Tile38Connection.putClauseFirst(clause, bound2, uri)
        val intersects2 = Tile38Connection.loadClausesInRegion(bound, uri)
        println("inters2: $intersects2")
        Tile38Connection.putClauseFirst(clause2, bound3, uri)
        val intersects3 = Tile38Connection.loadClausesInRegion(bound, uri)
        println("inters3: $intersects3")

        Tile38Connection.putClauseLast(clause4, bound, uri)
        val intersects4 = Tile38Connection.loadClausesInRegion(bound, uri)
        println(intersects4)
        Tile38Connection.putClauseFirst(clause5, bound, uri)
        val intersects5 = Tile38Connection.loadClausesInRegion(bound, uri)
        println(intersects5)

        println(Tile38Connection.loadClausesInRegion(bound2, uri))
        println(Tile38Connection.loadClausesInRegion(bound3, uri))


        //connection.dropAll()*/
    }
}