package it.unibo.tuprolog.geo.theory.region

import it.unibo.tuprolog.geo.theory.region.impl.Bounds
import it.unibo.tuprolog.geo.theory.region.impl.Here
import it.unibo.tuprolog.geo.theory.geoshape.GeoLocation

/** params for each region*/
sealed class RegionFactoryParam {
    data class HereParam(val point: GeoLocation, val radius: Int): RegionFactoryParam()
    data class BoundParam(val southWesternPoint: GeoLocation, val northEasternPoint: GeoLocation, val within: Boolean = false): RegionFactoryParam()
}

/** factory to create a region*/
object RegionFactory {
    fun getRegion(id: String, param: RegionFactoryParam): Region {
        return when (param) {
            is RegionFactoryParam.HereParam -> Here(
                id,
                param.point,
                param.radius
            )
            is RegionFactoryParam.BoundParam -> Bounds(
                id,
                param.southWesternPoint,
                param.northEasternPoint,
                param.within
            )
        }
    }
}