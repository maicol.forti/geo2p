package it.unibo.tuprolog.geo.theory.tile38

import it.unibo.tuprolog.geo.theory.tile38.tile38object.Tile38Object
import it.unibo.tuprolog.geo.theory.tile38.tile38object.Tile38ObjectFactory
import it.unibo.tuprolog.theory.parsing.ClausesParser
import org.json.JSONArray
import org.json.JSONObject

/** A parser of Tile38 objects*/
object Tile38Parser {
    private const val key = "Prolog"
    /** Parse the result of a Tile38 command into a list of Tile38 objects*/
    fun parseResult(result: List<Any?>?): List<Tile38Object> {
        val jsonArray = JSONArray(result.toString())[1] as JSONArray
        return if (jsonArray.isEmpty) {
            listOf()
        } else {
            val geoLocatedDataAsList =
                sortGeoJSONArray(jsonArray)
            geoLocatedDataAsList.filter { it.getJSONObject(1).has(key) }
            geoLocatedDataAsList.map {
                parseJSONObject(
                    it[0].toString(),
                    it.getJSONObject(1)
                )
            }
        }
    }

    private fun parseJSONObject(id: String, jsonObject: JSONObject): Tile38Object {
        val type = jsonObject["type"].toString()
        val coordinates = jsonObject["coordinates"].toString()
        val clauses =  ClausesParser.withDefaultOperators.parseClauses(jsonObject[key].toString())
        return Tile38ObjectFactory.getObject(
            type,
            id,
            coordinates,
            clauses
        )
    }
}